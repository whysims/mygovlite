import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../../service/rest-api.service';
import { AlertService } from '../../service/alert.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: string;
  public searchInput: any;
  public lottieConfig: Object;
  public loading = false;
  public searchList = [];
  public inputCheck: boolean;

  constructor(public route: ActivatedRoute, public router: Router,public restApi: RestApiService, public alertService: AlertService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.lottieConfig = {
      path: 'assets/animations/loading.json',
      autoplay: true,
      loop: true
    };
  }

  ngOnInit() {
  }

  public logout(): void {
    this.restApi.logout();
  }

  changeRoutes(value) {
    this.router.navigate([value]);
  }

  searchValue(value) {
    if (value.length <= 0) {
      this.inputCheck = false;
    } else {
      this.inputCheck = true;
    }
    this.loading = true;
    if (value.length >= 3) {
      this.restApi.search(value, this.currentUser).subscribe(
        data => {
          this.loading = false;
          this.searchList = data;
        },
        error => {
          this.loading = false;
          this.alertService.error = error;
        });
    };
  }
  viewSearchResult(type, id) {
    if(type === 'Evento') {
      this.router.navigate(['viewcontact', {param: id}])
    } else {
      this.router.navigate(['viewcontact/'+id])
    }
  }
}
