export class Contacts {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    city: string;
    state: string;
    street: string;
    zipcode: number;
    rg: number;
    cpf: number;
    exp: string;
    leader: number;
    phone: number;
    phone2: number;
    note: string;
    party: string;
}
