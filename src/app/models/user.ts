export class User {
    id: number;
    username: string;
    password: string;
    firstname: string;
    lastname: string;
    domain: string;
    last_log: DateTimeFormat;
    eula_accept: number;
    email: string;
}
