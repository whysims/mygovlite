import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppRouter } from './app.router';

import { LottieAnimationViewModule } from 'ng-lottie';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule } from 'angular-calendar';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { GuardDirective } from './guard/guard.directive';
import { AlertComponent } from './directive/alert/alert.component';
import { HomeComponent } from './home/home.component';

import { AlertService } from './service/alert.service';
import { RestApiService } from './service/rest-api.service';
import { PaginationService } from './service/pagination.service';
import { LoginComponent } from './login/login.component';
import { ContactsComponent } from './contacts/contacts.component';
import { EventsComponent } from './events/events.component';
import { CalendarComponent } from './shared/calendar/calendar.component';
import { ModalComponent } from './shared/modal/modal.component';
import { ViewcontactComponent } from './viewcontact/viewcontact.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AlertComponent,
    HomeComponent,
    LoginComponent,
    ContactsComponent,
    EventsComponent,
    CalendarComponent,
    ViewcontactComponent,
    
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    AppRouter,
    LottieAnimationViewModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot()
  ],
  providers: [
    GuardDirective,
    RestApiService,
    AlertService,
    PaginationService,
    ModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
