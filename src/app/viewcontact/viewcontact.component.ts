import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestApiService, AlertService } from '../service';
import { Contacts } from '../models/contacts';

@Component({
  selector: 'app-viewcontact',
  templateUrl: './viewcontact.component.html',
  styleUrls: ['./viewcontact.component.css']
})
export class ViewcontactComponent implements OnInit {
    private params: any;
    private currentUser: string;
    private loading = false;
    public lottieConfig: Object;
    private contact: Contacts;

  constructor(private route: ActivatedRoute, private restApi: RestApiService, private alertService: AlertService) {
    
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.route.params.subscribe(params => { this.params = params; this.setUserDetails(); });
    this.lottieConfig = {
      path: 'assets/animations/loadingbounce.json',
      autoplay: true,
      loop: true
    };
   }

  ngOnInit() {
  }

  setUserDetails() {
    this.loading = true;

    this.restApi.contacts(this.currentUser, 'byid', this.params.id).subscribe(
      data => {
        this.contact = data.data;
        this.loading = false;
      },
      error => {
        this.loading = false;
        this.alertService.error = error.msg;
      });
  }

}
