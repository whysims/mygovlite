import { Routes, RouterModule } from '@angular/router';
 
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { EventsComponent } from './events/events.component';
import { ContactsComponent } from './contacts/contacts.component';
import { ViewcontactComponent } from './viewcontact/viewcontact.component';
import { GuardDirective } from './guard/guard.directive';
 
const appRoutes: Routes = [
    { path: 'home', component: HomeComponent, canActivate: [GuardDirective] },
    { path: 'login', component: LoginComponent },
    { path: 'events', component: EventsComponent, canActivate: [GuardDirective] },
    { path: 'contacts', component: ContactsComponent, canActivate: [GuardDirective] },
    { path: 'viewcontact/:id', component: ViewcontactComponent, canActivate: [GuardDirective] },
    { path: '**', redirectTo: 'login' }
];
 
export const AppRouter = RouterModule.forRoot(appRoutes);