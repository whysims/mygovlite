import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { User } from '../models/user';
import { Observable } from 'rxjs/Observable';
import { catchError, retry } from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Injectable()
export class RestApiService {
  public API_URL = "http://mygov.techpublic.com.br/api/user.php";
  public API_CONTACTS = "http://mygov.techpublic.com.br/api/contacts.php";
  public API_SEARCH = "http://mygov.techpublic.com.br/api/search.php";
  private errorMessage: any;
  constructor(private _http: HttpClient) {

  }

  login(user: string, pass: string) {
    return this._http.post<any>(this.API_URL, { username: user, password: pass })
      .map(user => {
        if (user.data) {
          localStorage.setItem('currentUser', user.data);
        }

        return user;
      });

  }

  contacts(value, action, param) {
    return this._http.post<any>(this.API_CONTACTS, { domain: value.domain, action: action, param: param })
      .map(contacts => {
        return contacts;
      });

  }

  search(value, domain) {
    return this._http.post<any>(this.API_SEARCH, { domain: domain.domain, search: value })
      .map(results => {
        return results;
      });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');

  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };



}
