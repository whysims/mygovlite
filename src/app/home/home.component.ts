import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { ContactsComponent } from '../contacts/contacts.component';
import { EventsComponent } from '../events/events.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: string;

  constructor() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

  }

  ngOnInit() {
  }

}
