import { Component, OnInit, Input } from '@angular/core';
import { RestApiService } from '../service/rest-api.service';
import { AlertService } from '../service/alert.service';
import { Contacts } from '../models/contacts';
import { PaginationService } from '../service/pagination.service';
import * as _ from 'underscore';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  public contacts: Contacts[];
  public lottieConfig: Object;
  public loading = false;
  public retrievedDomain: string;
  private allItems: Contacts[];
  private selectedPag: any;
  pager: any = {};
  pagedItems: any[];
  private domain: any;
  private currentUser: string; 

  constructor(private restApi: RestApiService, private alertService: AlertService, private pagination: PaginationService) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.lottieConfig = {
      path: 'assets/animations/loading.json',
      autoplay: true,
      loop: true
    };

  }

  ngOnInit() {
    
    this.retrieveContacts();
  }

  retrieveContacts() {
    this.loading = true;
    this.restApi.contacts(this.currentUser, 'all', null).subscribe(
      data => {
        this.loading = false;
        this.allItems = data.data;
        this.setPage(1);
      },
      error => {
        this.loading = false;
        this.alertService.error = error;
      });
  };

  setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
      return;
    }
    this.pager = this.pagination.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
}
