import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../shared/header/header.component';

import { AppRouter } from '../app.router';
import { ActivatedRoute, Router } from '@angular/router';

import { AlertService } from '../service/alert.service';
import { RestApiService } from '../service/rest-api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public lottieConfig: Object;
  public returnUrl: string;
  model: any = {};
  loading = false;

  constructor(public restApi: RestApiService, public route: ActivatedRoute, public router: Router, public alertService: AlertService) {
    this.lottieConfig = {
      path: 'assets/animations/loading.json',
      autoplay: true,
      loop: true
    };
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'home';
  }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.restApi.login(this.model.username, this.model.password).subscribe(
      data => {
        this.loading = false;
        this.router.navigate([this.returnUrl]);
      },
      error => {
        this.loading = false;
        this.alertService.error = error;
      });
  };
}
